# HalertManager

[![CI Status](http://img.shields.io/travis/yovchev03@gmail.com/HalertManager.svg?style=flat)](https://travis-ci.org/yovchev03@gmail.com/HalertManager)
[![Version](https://img.shields.io/cocoapods/v/HalertManager.svg?style=flat)](http://cocoapods.org/pods/HalertManager)
[![License](https://img.shields.io/cocoapods/l/HalertManager.svg?style=flat)](http://cocoapods.org/pods/HalertManager)
[![Platform](https://img.shields.io/cocoapods/p/HalertManager.svg?style=flat)](http://cocoapods.org/pods/HalertManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HalertManager is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "HalertManager"
```

## Author

yovchev03@gmail.com, yovchev03@gmail.com

## License

HalertManager is available under the MIT license. See the LICENSE file for more info.
