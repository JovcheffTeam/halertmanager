//
//  AlertAction.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import UIKit

public struct AlertAction {
    let title: String?
    let style: AlertActionStyle
    let callback: () -> Void
    
    public init(title: String?, style: AlertActionStyle = AlertActionStyle.defaultStyle, callback: @escaping () -> Void) {
        self.title = title
        self.style = style
        self.callback = callback
    }
}

public struct AlertActionStyle {

    static var defaultStyle: AlertActionStyle = AlertActionStyle(backgroundColor: UIColor.gray,
                                                                 textColor: UIColor.black,
                                                                 textFont: UIFont.systemFont(ofSize: 16),
                                                                 borderWidth: 0,
                                                                 borderColor: UIColor.clear.cgColor,
                                                                 cornerRadius: 0)
    
    let backgroundColor: UIColor
    let textColor: UIColor
    let textFont: UIFont
    let borderWidth: CGFloat
    let borderColor: CGColor
    let cornerRadius: CGFloat
}
