//
//  BasicAlertController.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 2/21/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation

let popupAnimationSpeed = 0.15
let buttonHighlightedTextAlpha: CGFloat = 0.35

class BasicAlertController: UIViewController, ManagablePopuptController {

    static func getInstance() -> ManagablePopuptController? {
        return BasicAlertController()
    }
    
    /**
     BasicAlertController settings.
     Initialize these before calling AlertManager.shared.showCustom to see
     the effect
    */
    
    /// A subclass of BasicAlertButton to be used in the popup. 
    /// Create a subclass and override init(frame: CGFRect) applying 
    /// style setting after super.init(frame: frame) to create a custom experience
    static var buttonType: BasicAlertButton.Type = BasicAlertButton.self
    
    /// The desired default height of the buttons
    static var buttonHeight: CGFloat = 50
    
    /// The desired background color of the popup
    static var popupBackground: UIColor = .white
    
    /// The desired cornerRadius of the popup
    static var popupCornerRadius: CGFloat = 10

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var alertViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var baloonView: UIView!
    
    private var shouldBeep: Bool = false
    private var shouldVibrate: Bool = false
    private var soundID: SystemSoundID = 1007
    
    private var isLayouted = false
    private var actionIndex: Int?
    var message = ""
    var attributedMessage: NSMutableAttributedString?
    var buttonActions: [AlertAction] = []
    private var bottomMostFrame: CGRect?
    private let padding = CGFloat(20)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLabel.text = title
        if self.attributedMessage != nil {
            textLabel.attributedText = attributedMessage
        } else {
            textLabel.text = message
        }
        baloonView.backgroundColor = BasicAlertController.popupBackground
        baloonView.layer.cornerRadius = BasicAlertController.popupCornerRadius
        let width = titleLabel.frame.width
        titleLabel.sizeToFit()
        textLabel.sizeToFit()
        titleLabel.frame.size.width = width
        textLabel.frame.size.width = width
    }
    
    func reduceHeight() {
        // reducing the balloonView's height
        if title?.characters.count == 0 && titleLabel.frame.height == 0.0 {
            let reduceHeight = textLabel.frame.origin.y - titleLabel.frame.origin.y
            alertViewHeightConstraint.constant -= reduceHeight
            
            textLabel.frame.origin.y = titleLabel.frame.origin.y
            let c = textLabel.frame.origin.y
            baloonView.addConstraint(NSLayoutConstraint(item: textLabel,
                                                        attribute: .top,
                                                        relatedBy: .equal,
                                                        toItem: baloonView,
                                                        attribute: .top,
                                                        multiplier: 1.0,
                                                        constant: c))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Calculating the balloonView's height according to texts height and number of buttons to be addded
        alertViewHeightConstraint.constant = titleLabel.frame.height
            + textLabel.frame.height
            + ((BasicAlertController.buttonHeight + padding) * CGFloat(buttonActions.count))
            + (padding * 3)
        reduceHeight()
        
        for action in buttonActions {
            addButton(forAction: action)
        }
        UIView.animate(withDuration: popupAnimationSpeed) {
            self.view.alpha = 1
        }
    }
    
    func executeAction() {
        guard let actionIndex = actionIndex else { return }
        
        let action = buttonActions[actionIndex]
        action.callback()
        self.dismiss(animated: false) {
            AlertManager.shared.popFirstAlert()
        }
    }
    
    func setMessageObject(_ messageObject: AbstractMessage) {
        self.title = messageObject.title
        self.message = messageObject.message
        self.attributedMessage = messageObject.attributedMessage
        self.shouldBeep = messageObject.audioFeedback
        self.shouldVibrate = messageObject.vibroFeedback
        self.soundID = SystemSoundID(messageObject.systemSoundID ?? 1007)
    }
    
    func addTextField(configurationHandler: AlertViewTextInputHandler) {
        // TODO: Add textfield to AlertView and link it to the closure on "ok" press
    }
    
    func addActions(actions: [AlertAction]) {
        buttonActions = actions
    }
    
    private func addButton(forAction: AlertAction) {
        let button = BasicAlertController.buttonType.init(frame: createNextElementFrame())
        let style = forAction.style
        button.backgroundColor = style.backgroundColor
        button.setTitleColor(style.textColor, for: .normal)
        button.setTitleColor(style.textColor.withAlphaComponent(buttonHighlightedTextAlpha), for: .highlighted)
        button.titleLabel?.font = style.textFont
        button.layer.borderWidth = style.borderWidth
        button.layer.borderColor = style.borderColor
        button.layer.cornerRadius = style.cornerRadius
        
        button.appendAction(action: AlertAction(title: forAction.title, callback: { [weak self] in
            guard let strongSelf = self else { return }
            UIView.animate(withDuration: popupAnimationSpeed,
                           animations: {
                            strongSelf.view.alpha = 0
            },
                           completion: { _ in
                            strongSelf.dismiss {
                                forAction.callback()
                            }
            }
            )
        }))
        baloonView.addSubview(button)
    }
    
    private func createNextElementFrame() -> CGRect {
        if bottomMostFrame == nil {
            bottomMostFrame = CGRect(x: textLabel.frame.minX, y: textLabel.frame.maxY + padding, width: textLabel.frame.width, height: BasicAlertController.buttonHeight)
        } else {
            let frame = bottomMostFrame ?? CGRect()
            bottomMostFrame = CGRect(x: frame.minX, y: frame.maxY + padding, width: frame.width, height: frame.height)
        }
        return bottomMostFrame ?? CGRect()
    }
    
    func show() {
        if shouldBeep {
            AudioServicesPlaySystemSound(soundID)
        }
        if shouldVibrate {
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        }
        AlertManager.shared.topViewController()?.present(self, animated: false, completion: nil)
    }
    
    func dismiss(onCompletion: @escaping () -> Void) {
        dismiss(animated: false) {
            onCompletion()
            AlertManager.shared.popFirstAlert()
        }
    }
}
