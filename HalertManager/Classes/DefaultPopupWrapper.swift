//
//  DefaultPopupWrapper.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 4/28/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import UIKit
import AudioToolbox
import AVFoundation

class HalertSystemPopupWrapper: ManagableAlertView {
    
    func addTextField(configurationHandler: (UITextField) -> Void) {
        //...
    }

    private var alertTitle: String = ""
    private var message: String = ""
    private var shouldBeep: Bool = false
    private var shouldVibrate: Bool = false
    private var soundID: SystemSoundID = 1007
    
    private var actions: [AlertAction] = []
    
    var alertStyle: UIAlertControllerStyle?
    
    init() {}
    
    func setMessageObject(_ messageObject: AbstractMessage) {
        self.alertTitle = messageObject.title
        self.message = messageObject.message
        self.shouldBeep = messageObject.audioFeedback
        self.shouldVibrate = messageObject.vibroFeedback
        self.soundID = SystemSoundID(messageObject.systemSoundID ?? 1007)
    }
    
    func addActions(actions: [AlertAction]) {
        self.actions.append(contentsOf: actions)
    }
    
    func show() {
        let alert = UIAlertController(title: alertTitle,
                                      message: message,
                                      preferredStyle: alertStyle ?? .alert)
        _ = actions.map { action in
            alert.addAction(UIAlertAction(title: action.title ?? "", callback: action.callback))
        }
        if shouldBeep {
            AudioServicesPlaySystemSound(soundID)
        }
        if shouldVibrate {
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        }
        AlertManager.shared.topViewController()?.present(alert, animated: true)
    }
    
    func dismiss(onCompletion: @escaping () -> Void) {
        //...
    }
}

extension UIAlertAction {
    convenience init(title: String, style: UIAlertActionStyle = .default, callback: @escaping () -> Void) {
        self.init(title: title, style: style, handler: { action in
            _ = action
            callback()
            AlertManager.shared.popFirstAlert()
        })
    }
}
