//
//  NotificationViewController.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 4/24/17.
//  Copyright © 2017 Upnetix. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import AVFoundation

class InAppNotificationView: UIView, ManagableNotificationView {
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var notificationTextLabel: UILabel!
    
    // MARK: LOCAL CONSTANTS
    private static let buttonBorderWidth: CGFloat = 1
    private static let buttonBorderRadius: CGFloat = 3
    private static let buttonColor = UIColor.white
    private static let viewHeight: CGFloat = 70
    private let animationDuration = 0.33
    private static let onScreenTime: TimeInterval = 2
    private var soundID: SystemSoundID = 1007
    
    private var action: AlertAction?
    private (set) var animationType: AnimationType = .fly
    private var shouldVibrate: Bool = false
    private var shouldBeep: Bool   = false
    
    static func getView() -> ManagableNotificationView {
        let screenBounds = UIScreen.main.bounds
        let toastView = InAppNotificationView(frame: CGRect(x: 0, y: -viewHeight, width: screenBounds.width, height: viewHeight))
        let tap = UITapGestureRecognizer(target: toastView, action: #selector(forceHide))
        toastView.addGestureRecognizer(tap)
        toastView.button.layer.borderColor = buttonColor.cgColor
        toastView.button.layer.cornerRadius = buttonBorderRadius
        toastView.button.layer.borderWidth = buttonBorderWidth
        
        return toastView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        hm_setup(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        hm_setup(frame: self.frame)
    }
    
    func show() {
        AlertManager.shared.topViewController()?.view.addSubview(self)
        switch animationType {
        case .fade:
            showWithFade()
        case .fly:
            showWithFly()
        }
        if shouldBeep {
            AudioServicesPlaySystemSound(soundID)
        }
        if shouldVibrate {
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        }
    }
    
    func forceHide() {
        action = nil
        hide()
    }
    
    func hide() {
        switch animationType {
        case .fade:
            hideWithFade(action: action)
        case .fly:
            hideWithFly(action: action)
        }
    }
    
    func setMessageObject(_ messageObject: AbstractMessage) {
        self.shouldBeep = messageObject.audioFeedback
        self.shouldVibrate = messageObject.vibroFeedback
        self.soundID = SystemSoundID(messageObject.systemSoundID ?? 1007)
        self.notificationTextLabel.text = messageObject.message
    }
    
    func setAnimationStyle(style: AnimationType) {
        self.animationType = style
    }
    
    func addActions(actions: [AlertAction]) {
        if let customAction = actions.first {
            self.action = customAction
            button.setTitle(customAction.title, for: [])
        }
        button.isHidden = actions.isEmpty
        layoutIfNeeded()
    }

    private func showWithFly() {
        self.isHidden = false
        UIView.animate(withDuration: animationDuration, animations: { [weak self] in
            guard let strongSelf = self else { return }
            let oldFrame = strongSelf.frame
            strongSelf.frame = CGRect(x: oldFrame.minX, y: oldFrame.maxY, width: oldFrame.width, height: oldFrame.height)
            }, completion: { [weak self] _ in
                self?.hm_delay(InAppNotificationView.onScreenTime) {
                    self?.action = nil
                    self?.hide()
            }
        })
    }
    
    private func hideWithFly(action: AlertAction? = nil) {
        UIView.animate(withDuration: animationDuration, animations: { [weak self] in
            guard let strongSelf = self else { return }
            let oldFrame = strongSelf.frame
            strongSelf.frame = CGRect(x: oldFrame.minX, y: -InAppNotificationView.viewHeight, width: oldFrame.width, height: oldFrame.height)
        }, completion: { [weak self] _ in
            self?.dismiss {
                action?.callback()
            }
        })
    }
    
    func dismiss(onCompletion: @escaping () -> Void) {
        onCompletion()
        removeFromSuperview()
        AlertManager.shared.popFirstAlert()
    }
    
    private func showWithFade() {
        self.isHidden = false
        self.alpha = 0
        let oldFrame = frame
        self.frame = CGRect(x: oldFrame.minX, y: 0, width: oldFrame.width, height: oldFrame.height)
        UIView.animate(withDuration: animationDuration, animations: {
            self.alpha = 1
        }, completion: { [weak self] _ in
            self?.hm_delay(InAppNotificationView.onScreenTime) {
                self?.action = nil
                self?.hide()
            }
        })
    }
    
    private func hideWithFade(action: AlertAction? = nil) {
        UIView.animate(withDuration: animationDuration, animations: {
            self.alpha = 0
        }, completion: { [weak self] _ in
            self?.dismiss {
                action?.callback()
            }
        })
    }
    
    @IBAction func didTapActionButton(_ sender: Any) {
        hide()
    }
    
    enum AnimationType {
        case fade, fly
    }
    
}

// MARK: BOILERS
extension InAppNotificationView {
    /**
     Setup the view to match the size of the screen
     */
    fileprivate func hm_setup(frame: CGRect) {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)),
                        bundle: bundle)
        guard let mainView = nib.instantiate(withOwner: self,
                                             options: nil).first as? UIView else { return }
        self.frame = CGRect(x: 0, y: 0, width: mainView.frame.size.width, height: mainView.frame.size.height)
        self.addSubview(mainView)
        self.frame = frame
        mainView.translatesAutoresizingMaskIntoConstraints = false
        
        self.hm_pinTo(toView: mainView, attributes: .top, .bottom, .left, .right)
    }
    
    /**
     Pin the current view's sides to another view.
     @params: toView - the view to be pinned to
     @params: margin - the desired margin. 0 by default
     @params: attributes: the sides to which you want to pin the view
     */
    fileprivate func hm_pinTo(toView: UIView, margin: CGFloat = 0, attributes: NSLayoutAttribute...) {
        for side in attributes {
            let constraint = NSLayoutConstraint(item: self,
                                                attribute: side,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: toView,
                                                attribute: side,
                                                multiplier: 1.0,
                                                constant: margin)
            self.addConstraint(constraint)
        }
    }
    
    /**
     Function to delay a certain action.
     Use example:
     delay(delayTime) {
     code to be delayed..
     }
     */
    fileprivate func hm_delay(_ delay: Double, closure: @escaping () -> Void) {
        DispatchQueue
            .main
            .asyncAfter(deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC),
                        execute: closure)
    }
}
