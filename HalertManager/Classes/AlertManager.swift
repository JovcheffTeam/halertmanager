//
//  AlertManager.swift
//  HalertManager
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit


/// Classes implementing this interface should be able to show popup messages
public protocol CallableAlertManager {
    func showNotification(_ messageObject: AbstractMessage, action: AlertAction?)
    func showCustom(_ messageObject: AbstractMessage, actions: [AlertAction])
    func showSystem(_ messageObject: AbstractMessage, style: UIAlertControllerStyle, actions: [AlertAction])
    func showToast(_ messageObject: AbstractMessage, action: AlertAction?)
}

/// The protocol marks classes which can be passed to any of the AlertManager.show* methods and be used for presenting Alert Views
public protocol ManagableAlertView {
    
    /// Called on the view as a trigger for presenting on the screen. Predefined in the extension
    ///
    /// - Returns: Void
    func show()
    
    /// Adds an array of AlertAction objects to be appended to the Alert View for user interactions
    ///
    /// - Parameter actions: an array of AlertAction objects
    /// - Returns: Void
    func addActions(actions: [AlertAction])
    
    /// Method to setup the Alert View correspondingly to the message object
    ///
    /// - Parameter messageObject: AbstractMessage object carrying the data
    /// - Returns: Void
    func setMessageObject(_ messageObject: AbstractMessage)
    
    /// Called when an Alert View should be removed. Predefined in the extension
    /// to call AlertManager.shared.popFirstAlert() on completion so the Queue can be activated
    /// and the next Alert can be shown if any
    ///
    /// - Parameter onCompletion: closure to be executed on dismiss completion
    func dismiss(onCompletion: @escaping () -> Void)
}

public protocol ManagablePopuptController: ManagableAlertView {
    
    /// AlertManager uses this method to get an instance of the Custom Popup ViewController to be presented in .showCustom()
    ///
    /// - Returns: an instance of ManagablePopupController. Optional for convenience
    static func getInstance() -> ManagablePopuptController?
    
    /// Adds textfield to be shown in the popup
    ///
    /// - Parameter configurationHandler: AlertViewTextInputHandler a.k.a ((UITextField) -> Void)
    /// - Returns: Void
    func addTextField(configurationHandler: AlertViewTextInputHandler)
}

extension ManagablePopuptController where Self: UIViewController {
    func show() {
        AlertManager.shared.topViewController()?.present(self, animated: false, completion: nil)
    }
    
    func dismiss(onCompletion: @escaping () -> Void) {
        dismiss(animated: false) {
            onCompletion()
            AlertManager.shared.popFirstAlert()
        }
    }
}

public protocol ManagableNotificationView: ManagableAlertView {
    
    /// Getter of the notification view to be shown
    ///
    /// - Returns: an instance of ManagableNotificationView
    static func getView() -> ManagableNotificationView
}

/// Delegate receiving callbacks on certain system-importaint AlertManager events.
/// Useful when ActivityIndicator/Loader is present
protocol AlertManagerDelegate {
    
    /// Invoked when an alert is about to be shown by the AlertManager instance
    func alertWillShow()
}

typealias AlertViewTextInputHandler = ((UITextField) -> Void)

public class AlertManager: CallableAlertManager {
    
    /// The name of the notification posted evey time the AlertManager is about to present a new notification
    /// on the screen.
    static let willIntroduceAlertNotificationName = "AlertManagerWillShowAlertNotificatioName"
    
    private static var instance: AlertManager?
    private let delegate: AlertManagerDelegate?
    private var customNotificationType: ManagableNotificationView.Type
    private let customPopupType: ManagablePopuptController.Type
    private var alertsQueue: [ManagableAlertView] = []
    
    private init(delegate: AlertManagerDelegate? = nil,
                 notificationViewType: ManagableNotificationView.Type? = nil,
                 popupViewType: ManagablePopuptController.Type? = nil) {
        self.delegate = delegate
        self.customNotificationType = notificationViewType ?? InAppNotificationView.self
        self.customPopupType = popupViewType ?? BasicAlertController.self
    }
    
    public static var shared: AlertManager {
        assert(instance != nil, "Trying to access AlertManager.shared before invoking AlertManager.instantiate()")
        assert(instance?.customPopupType is UIViewController.Type, "AlertManager instantiated with  ManagableAlertController subtype which does not extend UIViewController")
        return instance ?? AlertManager()
    }
    
    /// Entry point for the AlertManager.shared instance
    ///
    /// - Parameter delegate: Optional AlertManagerDelegate instace to receive callbacks
    public static func instantiate(customNotificationViewType: ManagableNotificationView.Type? = nil,
                            customPopupControllerType: ManagablePopuptController.Type? = nil) {
        AlertManager.instance = AlertManager(notificationViewType: customNotificationViewType,
                                             popupViewType: customPopupControllerType)
    }
    
    /// Shows a notification-style view on top of screen, which does not block the UI and hides on tap
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - action: Optional AlertAction to be executed on eventual button tap
    public func showNotification(_ messageObject: AbstractMessage, action: AlertAction?) {
        let alertToast = customNotificationType.getView()
        
        var actionsToAdd: [AlertAction] = []
        if let act = action {
            actionsToAdd.append(act)
        }
        
        alertToast.setMessageObject(messageObject)
        alertToast.addActions(actions: actionsToAdd)
        addToQueue(alertToast)
    }
    
    /// Shows a custom style Popup which blocks the UI
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - actions: Array of AlertAction objects to be presented in the popup
    public func showCustom(_ messageObject: AbstractMessage, actions: [AlertAction]) {
        self.showAlertController(messageObject, actions: actions)
    }
    
    /// Shows a system style Popup
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - actions: Array of AlertAction objects to be presented in the popup
    public func showSystem(_ messageObject: AbstractMessage, style: UIAlertControllerStyle = .alert, actions: [AlertAction]) {
        let alertView = HalertSystemPopupWrapper()
        alertView.alertStyle = style
        var actionsToAppend = actions
        if actionsToAppend.isEmpty {
            actionsToAppend.append(AlertAction(title: "OK", callback: {}))
        }
        
        alertView.setMessageObject(messageObject)
        alertView.addActions(actions: actionsToAppend)
        addToQueue(alertView)
    }
    
    
    /// Shows an Android-like toast which does not block the UI
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - action: Optional AlertAction to be executed on eventual tap on the view
    public func showToast(_ messageObject: AbstractMessage, action: AlertAction?) {
        // TODO: Implement toast-like view
    }

    
    /// Shows Popup with UITextField for input
    ///
    /// - Parameters:
    ///   - messageObject: an AbstractMessage object
    ///   - textFieldHandler: Handler to handle the text input
    ///   - callback: Callback to be executed on "OK" tap
    func showAlertWithTextField(_ messageObject: AbstractMessage, textFieldHandler: ((UITextField) -> Void)?, callback: @escaping () -> Void) {
        let confirmationAction: AlertAction = AlertAction(title: "OK", callback: callback)
        self.showAlertController(messageObject, actions: [confirmationAction], textFieldHandler: textFieldHandler)
    }
    
    private func showAlertController(_ messageObject: AbstractMessage,
                                     actions: [AlertAction],
                                     textFieldHandler: AlertViewTextInputHandler? = nil) {
        var actionsToAppend = actions
        
        guard let alertView = customPopupType.getInstance() else { return }
        
        (alertView as? UIViewController)?.modalPresentationStyle = .overCurrentContext
        alertView.setMessageObject(messageObject)
        
        // TODO: Fix custom Alert class to handle textField enabling
        if let handler = textFieldHandler {
            alertView.addTextField(configurationHandler: handler)
            actionsToAppend.append(AlertAction(title: "Cancel", callback: {}))
        }
        // Checking if no actions are passed to AlertManager and adding default popup-close action with button labeled "OK"
        if actionsToAppend.isEmpty {
            actionsToAppend.append(AlertAction(title: "OK", callback: {}))
        }
        alertView.addActions(actions: actionsToAppend)
        addToQueue(alertView)
    }
    
    func popFirstAlert() {
        alertsQueue.removeFirst()
        guard let nextAlert = alertsQueue.first else { return }
        show(nextAlert)
    }
    
    private func addToQueue(_ alert: ManagableAlertView) {
        if alertsQueue.isEmpty {
            show(alert)
        }
        alertsQueue.append(alert)
    }
    
    private func show(_ alert: ManagableAlertView) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: ""), object: nil, userInfo: nil)
        alert.show()
    }
    
    func topViewController() -> UIViewController? {
        var topMostViewController: UIViewController?
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topMostViewController = topController
        }
        return topMostViewController
        
    }
}
