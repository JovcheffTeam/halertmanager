//
//  ServerMessage.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 2/22/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation

public class AbstractMessage {
    let title: String
    let message: String
    let attributedMessage: NSMutableAttributedString?
    let audioFeedback: Bool
    let vibroFeedback: Bool
    let systemSoundID: Int?

    public init(messagText: String,
         title: String?,
         attributedText: NSMutableAttributedString? = nil,
         sound: Bool = false,
         vibration: Bool = false,
         systemSoundID: Int? = nil) {
        
        self.message = messagText
        self.title = title ?? ""
        self.attributedMessage = attributedText
        self.audioFeedback = sound
        self.vibroFeedback = vibration
        self.systemSoundID = systemSoundID
    }
}
