//
//  BasicAlertButton.swift
//  PayTalkClient
//
//  Created by dyanko.yovchev on 2/21/17.
//  Copyright © 2017 dyanko.yovchev. All rights reserved.
//

import Foundation
import UIKit

class BasicAlertButton: UIButton {
    private var customActions: [AlertAction] = []
    
    private var shouldUseDefaultFontSettings: Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override required init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setup() {
        self.addTarget(self, action: #selector(executeActions), for: .touchUpInside)
    }
    
    /**
     Appends a custom action closure to the button to be executed before the touchesEnded is passed through the super. Also provides a
     custom title bound to the AlertAction object that sets a new title to the button, so if you want to add multiple actions to a single
     button, make sure to pass the correct title in the last added action in order to provide the button with the right name
     */
    func appendAction(action: AlertAction) {
        self.customActions.append(action)
        if let title = action.title {
            self.setTitle(title, for: .normal)
        }
    }
    
    @objc private func executeActions() {
        for action in customActions {
            action.callback()
        }
    }
}
