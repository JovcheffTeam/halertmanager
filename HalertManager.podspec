#
# Be sure to run `pod lib lint HalertManager.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HalertManager'
  s.version          = '0.1.0'
  s.summary          = 'Popups and toasts.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Manager that is able to show a set of highly customizable views on the screen with a queue managing the appearance of multiple alerts in the same time.
This library allows you to show on-screen default/custom alert popup, in-app-notification style popup, toast-like popup and more, each having the ability to add custom action
to be executed with corresponding mechanics for user interaction.
                       DESC

  s.homepage         = 'https://bitbucket.org/JovcheffTeam/halertmanager.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Dyanko Yovchev' => 'yovchev03@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/JovcheffTeam/halertmanager.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/Yovcheff'

  s.ios.deployment_target = '9.0'

  s.source_files = 'HalertManager/Classes/**/*'
  
#  s.resource_bundles = {
#    'HalertManager' => ['HalertManager/Assets/*.png']
#  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
